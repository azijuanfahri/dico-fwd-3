class FavoriteRestaurantShowPresenter {
    constructor({ view, favoriteRestaurants2 }) {
        this._view = view;
        this._favoriteRestaurants2 = favoriteRestaurants2;

        this._showFavoriteRestaurants();
    }

    async _showFavoriteRestaurants() {
        const restaurants = await this._favoriteRestaurants2.getAllRestaurants();
        this._displayRestaurants(restaurants);
    }

    _displayRestaurants(restaurants) {
        this._view.showFavoriteRestaurants(restaurants);
    }
}

export default FavoriteRestaurantShowPresenter;
