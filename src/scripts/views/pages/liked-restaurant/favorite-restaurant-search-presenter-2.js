class FavoriteRestaurantSearchPresenter2 {
    constructor({ favoriteRestaurants2, view }) {
        this._view = view;
        this._listenToSearchRequestByUser();
        this._favoriteRestaurants2 = favoriteRestaurants2;
    }

    _listenToSearchRequestByUser() {
        this._view.runWhenUserIsSearching((latestQuery) => {
            this._searchRestaurant2(latestQuery);
        });
    }


    _showFoundRestaurants(restaurants) {

        this._view.showFavoriteRestaurants(restaurants);
    }

    get latestQuery() {
        return this._latestQuery;
    }


    async _searchRestaurant2(latestQuery) {
        this._latestQuery = latestQuery.trim();

        let foundRestaurants;
        if (this.latestQuery.length > 0) {
            foundRestaurants = await this._favoriteRestaurants2.searchRestaurant2(this.latestQuery);
        } else {
            foundRestaurants = await this._favoriteRestaurants2.getAllRestaurants();
        }

        this._showFoundRestaurants(foundRestaurants)
    }
}

export default FavoriteRestaurantSearchPresenter2;
