import FavoriteRestaurantIdb from '../../data/favoriterestaurant-idb';
import RestaurantSource from '../../data/restaurant-source';
import {
  createRestaurantItemTemplate,
  createRestaurantFavoriteTemplate,
  createLoaderTemplate,
} from '../templates/template-creator';

const ListRestaurant = {
  async render() {
    const html = `${(document.querySelector('main').innerHTML =
      createLoaderTemplate.show())}
        <div class="hero">

        <picture>
             <source type="image/webp"
                media="(max-width: 320px)"
                srcset="images/heros/hero-image-small.webp">

             <source type="image/webp"
                media="(max-width: 500px)"
                srcset="images/heros/hero-image-medium.webp">

             <source type="image/webp"
                media="(max-width: 768px)"
                srcset="images/heros/hero-image-high.webp">

             <source type="image/webp"
                media="(max-width: 1000px)"
                srcset="images/heros/hero-image-large.webp">

             <img class="lazyload"
             data-src='images/heros/hero-image.jpg'
             alt="Background Title"></img>

        </picture>
        <div class="overlay">
        <div class="hero__inner">
            <h1 class="hero__title">Best restaurant apps that you need in 2021</h1>
            <p tabindex="0" class="hero__tagline">
                Although this may seem like another restaurant and attractions review
                web, believe it or not Hunger App offers some of the best reviews and
                recommendations for restaurants. I personally use this to look up the
                top restaurants in a certain area.
            </p>
        </div>
        </div>
    </div>

    <div class="favorite_space">
        <article class="favorite">
            <h1 class="favorite__tagline">Favorites Restaurants</h1>
            <h2 class="description__paragraph">If you press the like button on the detail page, then the page you like
                will appear here.</h2>
            <div id="fav" class="favorite_item">

            </div>
        </article>
    </div>

    <div class="explorer">
        <h1 class="explorer__label">Explore Restaurant</h1>
        <div id="data" class="restaurants">

        </div>
    </div>
      `;
    return html;
  },

  async afterRender() {
    const favRestaurants = await FavoriteRestaurantIdb.getAllRestaurants();
    const favContainer = document.querySelector('#fav');
    favRestaurants.forEach((favorite) => {
      favContainer.innerHTML += createRestaurantFavoriteTemplate(favorite);
    });

    const restaurants = await RestaurantSource.listRestaurant();
    const restaurantsContainer = document.querySelector('#data');
    restaurants.forEach((restaurant) => {
      restaurantsContainer.innerHTML +=
        createRestaurantItemTemplate(restaurant);
    });
    createLoaderTemplate.remove();
  },
};

export default ListRestaurant;
