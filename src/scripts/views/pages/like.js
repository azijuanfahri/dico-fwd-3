import FavoriteRestaurantIdb from '../../data/favoriterestaurant-idb';
import FavoriteRestaurantSearchView from './liked-restaurant/favorite-restaurant-search-view';
import FavoriteRestaurantShowPresenter from './liked-restaurant/favorite-restaurant-show-presenter';
import FavoriteRestaurantSearchPresenter2 from './liked-restaurant/favorite-restaurant-search-presenter-2';

const view = new FavoriteRestaurantSearchView();

const Like = {
    async render() {
        return view.getTemplate();
    },

    async afterRender() {
        new FavoriteRestaurantShowPresenter({ view, favoriteRestaurants2: FavoriteRestaurantIdb });
        new FavoriteRestaurantSearchPresenter2({ view, favoriteRestaurants2: FavoriteRestaurantIdb });
    },
};

export default Like;
