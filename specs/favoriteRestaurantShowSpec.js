import FavoriteRestaurantShowPresenter from "../src/scripts/views/pages/liked-restaurant/favorite-restaurant-show-presenter";
import FavoriteRestaurantSearchView from "../src/scripts/views/pages/liked-restaurant/favorite-restaurant-search-view";
import FavoriteRestaurantIdb from "../src/scripts/data/favoriterestaurant-idb";

describe('Showing all favorite restaurants', () => {
    let view;


    const renderTemplate = () => {
        view = new FavoriteRestaurantSearchView();
        document.body.innerHTML = view.getTemplate();

    };

    beforeEach(() => {
        renderTemplate();
    });


    describe('When no restaurants have been liked', () => {

        it('should ask for the favorite restaurants', () => {
            const favoriteRestaurants2 = spyOnAllFunctions(FavoriteRestaurantIdb);

            new FavoriteRestaurantShowPresenter({
                view,
                favoriteRestaurants2,
            });

            expect(favoriteRestaurants2.getAllRestaurants).toHaveBeenCalledTimes(1);
        });

        it('should show the information that no restaurants have been liked', (done) => {
            document.getElementById('restaurants').addEventListener('restaurants:updated', () => {
                expect(document.querySelectorAll('.restaurant-item__not__found').length)
                    .toEqual(1);

                done();
            });

            const favoriteRestaurants2 = spyOnAllFunctions(FavoriteRestaurantIdb);
            favoriteRestaurants2.getAllRestaurants.and.returnValues([]);

            new FavoriteRestaurantShowPresenter({
                view,
                favoriteRestaurants2,
            });
        });
    });

    describe('When favorite restaurants exist', () => {
        it('should show the restaurants', (done) => {
            document.getElementById('restaurants').addEventListener('restaurants:updated', () => {
                expect(document.querySelectorAll('.restaurant-item').length).toEqual(2);
                done();
            });

            const favoriteRestaurants2 = spyOnAllFunctions(FavoriteRestaurantIdb);
            favoriteRestaurants2.getAllRestaurants.and.returnValues([
                {
                    id: 11, title: 'A', vote_average: 3, overview: 'This is Restaurant A',
                },
                {
                    id: 22, title: 'B', vote_average: 4, overview: 'This is Restaurant B',
                },
            ]);

            new FavoriteRestaurantShowPresenter({
                view,
                favoriteRestaurants2,
            });
        });
    });
});
