import { itActsAsFavoriteRestaurantModel } from "./favoriteRestaurantContract";

let favoriteRestaurants2 = [];

const FavoriteRestaurantArray = {

    getRestaurant(id) {
        if (!id) {
            return;
        }

        return favoriteRestaurants2.find((restaurant) => restaurant.id === id);
    },

    getAllRestaurants() {
        return favoriteRestaurants2;
    },

    putRestaurant(restaurant) {
        if (!restaurant.hasOwnProperty('id')) {
            return;
        }

        // pastikan id ini belum ada dalam daftar favoriteRestaurants
        if (this.getRestaurant(restaurant.id)) {
            return;
        }

        favoriteRestaurants2.push(restaurant);
    },

    deleteRestaurant(id) {
        // cara boros menghapus film dengan meng-copy film yang ada
        // kecuali film dengan id == id
        favoriteRestaurants2 = favoriteRestaurants2.filter((restaurant) => restaurant.id !== id);
    },

    searchRestaurant2(query) {
        return this.getAllRestaurants()
            .filter((restaurant) => {
                const loweredCaseRestaurantTitle = (restaurant.title || '-').toLowerCase();
                const jammedRestaurantTitle = loweredCaseRestaurantTitle.replace(/\s/g, '');

                const loweredCaseQuery = query.toLowerCase();
                const jammedQuery = loweredCaseQuery.replace(/\s/g, '');

                return jammedRestaurantTitle.indexOf(jammedQuery) !== -1;
            });
    },
};

describe('Favorite Restaurant Array Contract Test Implementation', () => {
    afterEach(() => favoriteRestaurants2 = []);

    itActsAsFavoriteRestaurantModel(FavoriteRestaurantArray);
});
