import FavoriteRestaurantIdb from "../src/scripts/data/favoriterestaurant-idb";
import FavoriteRestaurantSearchPresenter2 from "../src/scripts/views/pages/liked-restaurant/favorite-restaurant-search-presenter-2";
import FavoriteRestaurantSearchView from "../src/scripts/views/pages/liked-restaurant/favorite-restaurant-search-view";


describe('Searching restaurants', () => {
    let presenter;
    let favoriteRestaurants2;
    let view;


    const searchRestaurant2 = (query) => {
        const queryElement = document.getElementById('query');
        queryElement.value = query;
        queryElement.dispatchEvent(new Event('change'));
    }

    const setRestaurantSearchContainer = () => {
        view = new FavoriteRestaurantSearchView();
        document.body.innerHTML = view.getTemplate();
    };

    const constructPresenter = () => {
        favoriteRestaurants2 = spyOnAllFunctions(FavoriteRestaurantIdb);
        presenter = new FavoriteRestaurantSearchPresenter2({
            favoriteRestaurants2,
            view,
        });
    };

    beforeEach(() => {
        setRestaurantSearchContainer();
        constructPresenter();
    });

    describe('When query is not empty', () => {


        it('should be able to capture the query typed by the user', () => {

            searchRestaurant2('restaurant a');

            expect(presenter.latestQuery).toEqual('restaurant a');
        });

        it('should ask the model to search for liked restaurant', () => {

            searchRestaurant2('restaurant a');

            expect(favoriteRestaurants2.searchRestaurant2)
                .toHaveBeenCalledOnceWith('restaurant a');
        });

        it('should show the found restaurants', () => {

            presenter._showFoundRestaurants([{ id: 1 }]);

            expect(document.querySelectorAll('.restaurant-item').length).toEqual(1);


            presenter._showFoundRestaurants([{ id: 1, title: 'Test1' }, { id: 2, title: 'Test2' }]);

            expect(document.querySelectorAll('.restaurant-item').length).toEqual(2);
        });
    });


    describe('When query is empty', () => {
        it('should capture the query as empty', () => {
            searchRestaurant2(' ');
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('    ');
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('');
            expect(presenter.latestQuery.length).toEqual(0);

            searchRestaurant2('\t');
            expect(presenter.latestQuery.length).toEqual(0);
        })

        it('should show all favorite restaurants', () => {

            searchRestaurant2('    ');
            expect(favoriteRestaurants2.getAllRestaurants)
                .toHaveBeenCalled();

        })
    });

    describe('when no favorite restaurants could be found', () => {

        it('should show the empty message', (done) => {
            document.getElementById('restaurants')
                .addEventListener('restaurants:updated', () => {
                    expect(document.querySelectorAll('.restaurant-item__not__found').length)
                        .toEqual(1);
                    done();
                });

            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a')
                .and
                .returnValues([]);

            searchRestaurant2('restaurant a')
        });

        it('should not show any restaurants', (done) => {
            document.getElementById('restaurants')
                .addEventListener('restaurants:updated', () => {
                    expect(document.querySelectorAll('.restaurant-item').length).toEqual(0);
                    done();
                });

            favoriteRestaurants2.searchRestaurant2.withArgs('restaurant a')
                .and
                .returnValues([]);

            searchRestaurant2('restaurant a');
        })
    })
});
