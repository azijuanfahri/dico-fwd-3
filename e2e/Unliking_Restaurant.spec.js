Feature('Unliking Restaurants');

const assert = require('assert');


Before(({ I }) => {
    I.amOnPage('/#/like');
});

Scenario('showing empty liked restaurants on page', ({ I }) => {
    I.see('Tidak ada restaurant untuk ditampilkan', '.restaurant-item__not__found');
});


Scenario('unliking one restaurant', async ({ I }) => {
    I.see('Tidak ada restaurant untuk ditampilkan', '.restaurant-item__not__found');

    I.amOnPage('/');

    I.seeElement('.restaurant-item__title a');

    const firstRestaurant = locate('.restaurant-item__title a').first();
    I.click(firstRestaurant);
    I.seeElement('#likeButton');
    I.click('#likeButton');

    I.amOnPage('/#/like');
    I.seeElement('.restaurant-item__title a');
    I.click(firstRestaurant);
    I.seeElement('#likeButton');
    I.click('#likeButton');

    I.amOnPage('/#/like');
    I.seeElement('#restaurants');

    const conditionTest = 'Tidak ada restaurant untuk ditampilkan';
    const noResto = await I.grabTextFrom('#restaurants');

    assert.strictEqual(noResto, conditionTest);
});
