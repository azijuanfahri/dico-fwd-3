Feature('Showing Favorite Restaurants on Homepage');

const assert = require('assert');


Before(({ I }) => {
    I.amOnPage('/');
});


Scenario('liking one restaurant to homepage', async ({ I }) => {
    I.dontSeeElement('.favorite1 a');

    I.seeElement('.restaurant-item__title a');

    const firstRestaurant = locate('.restaurant-item__title a').first();
    const firstRestaurantTitle = await I.grabTextFrom(firstRestaurant);
    I.click(firstRestaurant);

    I.seeElement('#likeButton');
    I.click('#likeButton');

    I.amOnPage('/');
    I.seeElement('.favorite');

    const likedRestaurantTitle = await I.grabTextFrom('.favorite1 a');

    assert.strictEqual(firstRestaurantTitle, likedRestaurantTitle);
});
